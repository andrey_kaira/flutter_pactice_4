import 'package:flutter/material.dart';

class Gallery {
  String path;
  String title;
  String subTitle;

  Gallery({
    this.title,
    this.path,
    this.subTitle,
  });
}
