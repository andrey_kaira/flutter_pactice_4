import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutterpractice/redux/state/state_navigate.dart';
import 'package:flutterpractice/screen/my_app.dart';
import 'package:flutterpractice/screen/page_2.dart';
import 'package:flutterpractice/screen/page_3.dart';
import 'package:flutterpractice/screen/page_4.dart';
import 'package:redux/redux.dart';

class NavigationSelector{
  static void Function() navigateToActionMyApp(Store<NavigationRouteState> store){
    return ()=>store.dispatch(
      NavigateToAction.replace(
        MyApp.ROUTED_NAME,
      ),
    );
  }
  static void Function() navigateToActionPage2(Store<NavigationRouteState> store){
    return ()=>store.dispatch(
      NavigateToAction.replace(
        Page2.ROUTED_NAME,
      ),
    );
  }
  static void Function() navigateToActionPage3(Store<NavigationRouteState> store){
    return ()=>store.dispatch(
      NavigateToAction.replace(
        Page3.ROUTED_NAME,
      ),
    );
  }
  static void Function() navigateToActionPage4(Store<NavigationRouteState> store){
    return ()=>store.dispatch(
      NavigateToAction.replace(
        Page4.ROUTED_NAME,
      ),
    );
  }
}