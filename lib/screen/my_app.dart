import 'package:flutter/material.dart';
import 'package:flutterpractice/widgets/my_drawer.dart';

import '../res.dart';

class MyApp extends StatefulWidget {
  static const String ROUTED_NAME = '/my-app';

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp>  with NavigatorObserver{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 1'),
      ),
      drawer: MyDrawer(),
      body: Container(
        color: Colors.green,
        child: Padding(
          padding: EdgeInsets.all(10.0),
          child: GridView(
            children: gallery
                .map(
                  (gal) => GridTile(
                    child: FadeInImage(
                      fit: BoxFit.fitHeight,
                      placeholder: AssetImage('images/load.jpg'),
                      image: NetworkImage(
                        gal.path,
                      ),
                    ),
                    footer: GridTileBar(
                      backgroundColor: Colors.black54,
                      title: Text(
                        gal.title,
                      ),
                      subtitle: Text(
                        gal.subTitle,
                      ),
                    ),
                  ),
                )
                .toList(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.6,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
            ),
          ),
        ),
      ),
    );
  }
}
