import 'package:flutter/material.dart';
import 'package:flutterpractice/widgets/my_drawer.dart';

class Page4 extends StatelessWidget {
  static const String ROUTED_NAME = '/page-4';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 4'),
      ),
      drawer: MyDrawer(),
      body: Container(
        color: Colors.blue,
      ),
    );
  }
}
