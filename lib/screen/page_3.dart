import 'package:flutter/material.dart';
import 'package:flutterpractice/widgets/my_drawer.dart';

class Page3 extends StatelessWidget {
  static const String ROUTED_NAME = '/page-3';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 3'),
      ),
      drawer: MyDrawer(),
      body: Container(
        color: Colors.yellow,
      ),
    );
  }
}
