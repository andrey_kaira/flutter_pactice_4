import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutterpractice/applications/application.dart';
import 'package:flutterpractice/widgets/my_drawer.dart';

class Page2 extends StatefulWidget {
  static const String ROUTED_NAME = '/page-2';

  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> with NavigatorObserver{
  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {
    print('This is never get called');
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) {
    print('This is never get called');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 2'),
      ),
      drawer: MyDrawer(),
      body: Container(
        alignment: Alignment.center,
        color: Colors.red,
        child: Container(
          height: 50,
          width: 200,
          child: RaisedButton(
            child: Text('Pop'),
            onPressed: (){
              Navigator.of(context).pop();
            },
          ),
        ),
      ),
    );
  }
}
