import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'file:///C:/Users/AppVesto/AndroidStudioProjects/flutter_practice/lib/redux/reducers/reducer_navigate.dart';
import 'file:///C:/Users/AppVesto/AndroidStudioProjects/flutter_practice/lib/redux/state/state_navigate.dart';
import 'package:flutterpractice/screen/page_2.dart';
import 'package:flutterpractice/screen/page_3.dart';
import 'package:flutterpractice/screen/page_4.dart';
import 'package:redux/redux.dart';
import '../screen/my_app.dart';

final store = new Store<NavigationRouteState>(combineReducers<NavigationRouteState>([appReducer]),
    initialState: NavigationRouteState.initial(),
    middleware: [
      NavigationMiddleware<NavigationRouteState>(),
    ]);

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  @override
  Widget build(BuildContext context) {
    final RouteObserver<PageRoute> routeObserver = new RouteObserver<PageRoute>();
    return StoreProvider<NavigationRouteState>(
        store: store,
        child: MaterialApp(
          theme: ThemeData.dark(),
          navigatorKey: NavigatorHolder.navigatorKey,
          home: MyApp(),
          navigatorObservers: <NavigatorObserver>[routeObserver],
          routes: {
            MyApp.ROUTED_NAME:(context) => MyApp(),
            Page2.ROUTED_NAME:(context) => Page2(),
            Page3.ROUTED_NAME:(context) => Page3(),
            Page4.ROUTED_NAME:(context) => Page4(),
          },
        )
    );
  }
}
