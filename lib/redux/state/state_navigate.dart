class NavigationRouteState {
  final String name;

  const NavigationRouteState(this.name);

  factory NavigationRouteState.initial() => NavigationRouteState(null);
  factory NavigationRouteState.changeName(String name) => NavigationRouteState(name);
}