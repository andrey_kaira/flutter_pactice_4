import 'package:flutter/material.dart';
import 'package:flutterpractice/screen/my_app.dart';

class YourPageWidgetState with NavigatorObserver {
  // Called when the top route has been popped off, and the current route shows up.
  void didPopNext() {
    print("didPopNext");
  }

  // Called when the current route has been pushed.
  void didPush(Route route,Route prevRoute) {
    print("didPush ${route.runtimeType}");
  }

  // Called when the current route has been popped off.
  void didPop(Route route,Route prevRoute) {
    print("didPop ${route.runtimeType}");
  }

  // Called when a new route has been pushed, and the current route is no longer visible.
  void didPushNext() {
    print("didPushNext");
  }
}