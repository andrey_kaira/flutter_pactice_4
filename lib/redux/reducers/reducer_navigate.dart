import '../actions/actions_navigate.dart';
import '../state/state_navigate.dart';
import 'package:redux/redux.dart';


NavigationRouteState _onAppNameChanged(NavigationRouteState state, AppNameChangedAction action) =>
    NavigationRouteState.changeName(action.name);

final appReducer = combineReducers<NavigationRouteState>([
  TypedReducer<NavigationRouteState, AppNameChangedAction>(_onAppNameChanged),
]);
