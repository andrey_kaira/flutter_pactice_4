class AppNameChangedAction {
  final String name;

  AppNameChangedAction(this.name);
}