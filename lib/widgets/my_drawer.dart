import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutterpractice/applications/application.dart';
import 'package:flutterpractice/screen/load_screen.dart';
import 'package:flutterpractice/screen/my_app.dart';
import 'package:flutterpractice/screen/page_2.dart';
import 'package:flutterpractice/screen/page_3.dart';
import 'package:flutterpractice/screen/page_4.dart';

class MyDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          AppBar(
            automaticallyImplyLeading: false,
          ),
          ListTile(
            title: Container(
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                gradient: LinearGradient(
                  colors: [
                    Colors.green,
                    Colors.lightGreenAccent,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Center(
                child: Text(
                  'Page 1',
                  style: TextStyle(fontSize: 50),
                ),
              ),
            ),
            onTap: () {
              store.dispatch(
                NavigateToAction.push(
                  MyApp.ROUTED_NAME,
                ),
              );
            },
          ),
          ListTile(
            title: Container(
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                gradient: LinearGradient(
                  colors: [
                    Colors.red,
                    Colors.pinkAccent,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Center(
                child: Text(
                  'Page 2',
                  style: TextStyle(fontSize: 50),
                ),
              ),
            ),
            onTap: () {
              store.dispatch(
                NavigateToAction.push(
                  Page2.ROUTED_NAME,
                ),
              );
            },
          ),
          ListTile(
            title: Container(
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                gradient: LinearGradient(
                  colors: [
                    Colors.amber,
                    Colors.yellowAccent,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Center(
                child: Text(
                  'Page 3',
                  style: TextStyle(fontSize: 50),
                ),
              ),
            ),
            onTap: () {
              store.dispatch(
                NavigateToAction.push(
                  Page3.ROUTED_NAME,
                ),
              );
            },
          ),
          ListTile(
            title: Container(
              height: 100.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                gradient: LinearGradient(
                  colors: [
                    Colors.blueAccent,
                    Colors.cyanAccent,
                  ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                ),
              ),
              child: Center(
                child: Text(
                  'Page 4',
                  style: TextStyle(fontSize: 50),
                ),
              ),
            ),
            onTap: () {
              store.dispatch(
                NavigateToAction.push(
                  Page4.ROUTED_NAME,
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
