import 'models/gallery.dart';

List<Gallery> gallery = [
  Gallery(
    title: 'Coco Palm Beach Resort',
    subTitle: 'Best spa',
    path: 'https://klike.net/uploads/posts/2019-07/1562069947_1.jpg',
  ),
   Gallery(
    title: '30 Best Beach Quotes ',
    subTitle: 'Sayings and Quotes About the Beach',
    path: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/beach-quotes-1559667853.jpg?crop=1.00xw:0.751xh;0,0.202xh&resize=1200:*',
  ),
   Gallery(
    title: 'Ikaros Beach Resort Spa Hotel',
    subTitle: 'luxury resort malia heraklion',
    path: 'https://www.ikarosvillage.gr/media/1282/pool.jpg?center=0.4625,0.49666666666666665&mode=crop&width=1680&height=1050&rnd=132116419150000000&quality=75',
  ),
   Gallery(
    title: 'Beach Glow',
    subTitle: '',
    path: 'https://store-images.s-microsoft.com/image/apps.49068.14458991885146991.028b24d1-0638-459d-bdfd-a6955fe1edf1.4b558854-e8dc-439e-84e1-7506fa8358a5?mode=scale&q=90&h=1080&w=1920',
  ),
   Gallery(
    title: 'Bomo Tosca Beach',
    subTitle: '(Bomo Tosca Beach) ',
    path: 'https://cdn.mouzenidis-travel.ru/static/userfiles/hotels/original/6133337/6133337_2pzylqim.4ym.jpg?preset=b',
  ),
   Gallery(
    title: 'The 24 Best Island Beaches in the World',
    subTitle: 'Condé Nast Traveler',
    path: 'https://media.cntraveler.com/photos/5c61c898b122107d7ad1aa59/master/w_4000,h_2650,c_limit/Nantucket_GettyImages-656291711.jpg',
  ),
   Gallery(
    title: 'Best 15 Beaches in Cebu Philippines',
    subTitle: '',
    path: 'https://gttp.imgix.net/224600/x/0/best-15-beaches-in-cebu-philippines-2.jpg?auto=compress%2Cformat&ch=Width%2CDPR&dpr=1&ixlib=php-2.3.0&w=883',
  ),
  Gallery(
    title: 'Ikaros Beach Resort Spa Hotel',
    subTitle: 'luxury resort malia heraklion',
    path: 'https://www.ikarosvillage.gr/media/1282/pool.jpg?center=0.4625,0.49666666666666665&mode=crop&width=1680&height=1050&rnd=132116419150000000&quality=75',
  ),
];
